<?php
/**
 * @file
 * pester.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pester_views_api() {
  return array("api" => "3.0");
}
