<?php
/**
 * @file
 * pester_watchdog.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pester_watchdog_views_api() {
  return array("version" => "3.0");
}
